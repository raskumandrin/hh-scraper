#!/usr/bin/perl -w
use strict;
use Mojo::UserAgent;
use Mojo::JSON;
use Data::Dumper;

use utf8;
binmode STDOUT, ":utf8";
   
my $hh_json;
my $json = Mojo::JSON->new;

my %vacancy;
# ключ - id вакансии, значение - id фирмы
my %firm;
# ключ - id фирмы, значение - название фирмы
my %firm_vacancy_count;
# ключ - id фирмы, значение - число вакансий по ней

# число вакансий на страницу
my $page_items_count = 100;

open my $fh,'<','spec-data.txt';
while (my $spec_string = <$fh>) {
	my ($spec,$field) = ( $spec_string =~ /specialization id="(\d+)" field="(\d+)"/ );
	next unless $spec and $field;
	
	print "spec: $spec, field: $field\n";

	my $base_url = qq(http://api.hh.ru/1/json/vacancy/search/?region=1\&field=$field&specialization=$spec);

	# организуем цикл, обходим страницы
	my $page = 0;
	my $vacancy_count = 0;
	do {
		my $url = "$base_url\&items=$page_items_count\&page=$page";
	
		print "GET $url\n";

		my $ua = Mojo::UserAgent->new;
		my $tx = $ua->get( $url );
		if ( my $res = $tx->success ) {
			print "ok.\n";
		#	print $res->body;
			$hh_json = $json->decode($res->body);
		}
		else {
			my ($err, $code) = $tx->error;
			die $code ? "$code response ($url): $err" : "Connection error ($url): $err";
		}
		$tx = undef;	
		$ua = undef;
	
		# обходим массив полученных вакансий
		foreach my $vac ( @{$hh_json->{vacancies}} ) {
			my $employer_id = $vac->{employer}->{id} || 0;
#			print "  id: $vac->{id}; firm id: $employer_id; firm name: $vac->{employer}->{name}\n";
			$vacancy{ $vac->{id} } = $employer_id;
			$firm{ $employer_id } = $vac->{employer}->{name};
		}
	
		# считаем вакансии, собранные за эту итерацию
		$vacancy_count += scalar @{$hh_json->{vacancies}};
		print "\n\$vacancy_count: $vacancy_count\n\n";
		$page++;
	} while ( $vacancy_count < $hh_json->{found} );

}
close $fh;


foreach (keys %vacancy) {
	$firm_vacancy_count{ $vacancy{$_} } ++;
}

#print "\nСписок фирм с вакансиями: < идентификатор фирмы ; название фирмы ; число вакансий >\n\n";

open $fh,'>','result.csv';
foreach my $firm_id (keys %firm) {
	$firm{$firm_id} =~ s/'/\'/g;

	print $fh "$firm_id\t$firm{$firm_id}\t$firm_vacancy_count{$firm_id}\thttp://hh.ru/employer/$firm_id\n"
}
close $fh;
